from flask import Flask, render_template, request
from werkzeug.utils import secure_filename
import os

app = Flask(__name__)

path = "C:/Users/binary flame/PycharmProjects/day2fileupload/ufiles"


@app.route('/')
def upload():
    return render_template('uploadui.html')


@app.route('/main', methods=['GET', 'POST'])
def upload_file():
    if request.method == 'POST':
        f = request.files['file']
        f.save(secure_filename(os.path.join(path, f.filename)))
        print(str(f))
        return 'File Uploaded Successfully :)'


if __name__ == '__main__':
    app.run(debug=True)